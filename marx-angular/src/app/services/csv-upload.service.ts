import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CsvUploadService {
  private baseUrl = 'http://localhost:8080/api/coherencyReports';

  constructor(private http: HttpClient) { }

  uploadCsvFile(csvContent: string) {
    return this.http.post<any>(`${this.baseUrl}/batch`, csvContent);
  }
}
