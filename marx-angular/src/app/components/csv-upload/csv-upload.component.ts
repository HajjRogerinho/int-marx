import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CsvUploadService } from 'src/app/services/csv-upload.service';

@Component({
  selector: 'app-csv-upload',
  templateUrl: './csv-upload.component.html',
  styleUrls: ['./csv-upload.component.css']
})
export class CsvUploadComponent {
  selectedFile: File | undefined;

  constructor(private csvUploadService: CsvUploadService,
              private router: Router) { }

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
  }

  uploadFile() {
    if (this.selectedFile) {
      const reader = new FileReader();
      reader.onload = () => {
        const fileContent = reader.result as string;
        this.csvUploadService.uploadCsvFile(fileContent).subscribe(
          (response) => {
            console.log('File uploaded successfully:', response);
            this.router.navigate(['/coherency-reports']);
          },
          (error) => {
            console.error('Error uploading file:', error);
          }
        );
      };
      reader.readAsText(this.selectedFile);
    }
  }
}
