import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CoherencyReport } from 'src/app/models/coherency-report.model';
import { CoherencyReportService } from 'src/app/services/coherency-report.service';
import options from '../../../../../options.json';

@Component({
  selector: 'app-coherency-reports-list',
  templateUrl: './coherency-reports-list.component.html',
  styleUrls: ['./coherency-reports-list.component.css']
})
export class CoherencyReportsListComponent implements OnInit {

  coherencyReports?: CoherencyReport[];

  serZoneFilter = '';
  typeFilter = '';
  tagFilter = '';
  descriptionFilter = '';

  availableSerZones = options.serZoneOptions;
  availableTags = options.tagOptions;
  availableTypes = options.typeOptions;

  constructor(private coherencyReportService: CoherencyReportService,
              private router: Router) { }

  ngOnInit(): void {
    this.retrieveCoherencyReports();
  }

  retrieveCoherencyReports(): void {
    this.coherencyReportService.getAllCoherencyReports()
      .subscribe({
        next: (data) => {
          this.coherencyReports = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }

  searchByAttributes(): void {
    this.coherencyReportService.searchByAttributes(this.serZoneFilter, this.typeFilter, this.tagFilter, this.descriptionFilter)
      .subscribe({
        next: (data) => {
          this.coherencyReports = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }

  updateCoherencyReport(coherencyReport: any): void {
    if (coherencyReport.id) {
      this.router.navigate(['/coherency-reports', coherencyReport.id]);
    } else {
      console.error('No ID found for the Coherency Report');
    }
  }

  deleteCoherencyReport(coherencyReport: any): void {
    this.coherencyReportService.deleteCoherencyReport(coherencyReport.id!)
      .subscribe({
        next: (res) => {
          console.log(res);
          this.retrieveCoherencyReports();
        },
        error: (e) => console.error(e)
      });
  }

  deleteAllCoherencyReports(): void {
    this.coherencyReportService.deleteAllCoherencyReports()
      .subscribe({
        next: (res) => {
          console.log(res);
          this.retrieveCoherencyReports();
        },
        error: (e) => console.error(e)
      });
  }

  clearSearchField(): void {
    this.serZoneFilter = '';
    this.typeFilter = '';
    this.tagFilter = '';
    this.descriptionFilter = '';

    this.searchByAttributes();
  }
}
