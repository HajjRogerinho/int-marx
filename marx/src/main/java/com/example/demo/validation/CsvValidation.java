package com.example.demo.validation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;

public class CsvValidation {
    public static boolean validateCsvContent(String csvContent) {
        try {
            String content = new String(Files.readAllBytes(Paths.get("C:\\Users\\bvp0101\\OneDrive - INFRABEL\\Documents\\Marx\\options.json")));
            JSONObject enums = new JSONObject(content);

            String[] serZoneOptions = toStringArray(enums.getJSONArray("serZoneOptions"));
            String[] typeOptions = toStringArray(enums.getJSONArray("typeOptions"));
            String[] tagOptions = toStringArray(enums.getJSONArray("tagOptions"));

            BufferedReader reader = new BufferedReader(new StringReader(csvContent));
            String line;
            boolean headerValidated = false;

            while ((line = reader.readLine()) != null) {
                if (!headerValidated) {
                    if (!line.equals("serZone;type;tag;description")) {
                        return false;
                    }
                    headerValidated = true;
                } else {
                    System.out.println(line);
                    String[] data = line.split(";");
                    if (data.length != 4) {
                        return false;
                    }
                    String serZone = data[0];
                    String type = data[1];
                    String tag = data[2];

                    if (!Arrays.asList(serZoneOptions).contains(serZone) ||
                        !Arrays.asList(typeOptions).contains(type) ||
                        !Arrays.asList(tagOptions).contains(tag)) {
                        return false;
                    }
                }
            }
            reader.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static String[] toStringArray(JSONArray jsonArray) {
        String[] array = new String[jsonArray.length()];
        for (int i = 0; i < jsonArray.length(); i++) {
            array[i] = jsonArray.getString(i);
        }
        return array;
    }
}
