package com.example.demo.repository;

import com.example.demo.model.CoherencyReport;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface CoherencyReportRepository extends CrudRepository<CoherencyReport, Integer> {
    @Query(value = "SELECT * FROM coherency_reports WHERE "
                   + "(:serZone IS NULL OR ser_zone = :serZone) AND "
                   + "(:type IS NULL OR type = :type) AND "
                   + "(:tag IS NULL OR tag = :tag) AND"
                   + "(:description IS NULL OR :description % ANY(STRING_TO_ARRAY(description,' ')))", nativeQuery = true)
    List<CoherencyReport> searchByAttributes(
            @Param("serZone") String serZone,
            @Param("type") String type,
            @Param("tag") String tag,
            @Param("description") String description
    );
}
