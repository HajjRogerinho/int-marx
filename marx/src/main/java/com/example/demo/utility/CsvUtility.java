package com.example.demo.utility;

import com.example.demo.model.CoherencyReport;
import java.io.BufferedReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class CsvUtility {
    public static String convertCsvToJson(String csvContent) {
        try {
            BufferedReader reader = new BufferedReader(new StringReader(csvContent));
            String line;
            String[] headers = null;
            JSONArray jsonArray = new JSONArray();

            while ((line = reader.readLine()) != null) {
                if (headers == null) {
                    headers = line.split(";");
                } else {
                    String[] data = line.split(";");
                    JSONObject obj = new JSONObject();
                    for (int i = 0; i < headers.length; i++) {
                        obj.put(headers[i], data[i]);
                    }
                    jsonArray.put(obj);
                }
            }

            reader.close();
            return jsonArray.toString();
        } catch (Exception e) {
            return "Error: Unable to read the file.";
        }
    }

    public static List<CoherencyReport> convertJsonToList(String json) {
        List<CoherencyReport> coherencyReports = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(json);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                CoherencyReport coherencyReport = new CoherencyReport(
                        jsonObject.getString("serZone"),
                        jsonObject.getString("type"),
                        jsonObject.getString("tag"),
                        jsonObject.getString("description"),
                        ""
                );
                coherencyReports.add(coherencyReport);
            }
        } catch (Exception e) {
            return new ArrayList<>();
        }
        return coherencyReports;
    }
}
