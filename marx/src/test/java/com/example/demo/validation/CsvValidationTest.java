package com.example.demo.validation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CsvValidationTest {

    @Test
    void testValidateCsvContent_ValidContent() {
        String csvContent = "serZone;type;tag;description\n" +
                            "FBMZ;catenaryCase without nodes;;21815";

        boolean isValid = CsvValidation.validateCsvContent(csvContent);

        assertTrue(isValid);
    }

    @Test
    void testValidateCsvContent_InvalidHeader() {
        String csvContent = "invalidHeader\n" +
                            "validSerZone;validType;validTag;validDescription";

        boolean isValid = CsvValidation.validateCsvContent(csvContent);

        assertFalse(isValid);
    }

    @Test
    void testValidateCsvContent_InvalidDataLength() {
        String csvContent = "serZone;type;tag;description\n" +
                            "validSerZone;validType;validTag";

        boolean isValid = CsvValidation.validateCsvContent(csvContent);

        assertFalse(isValid);
    }

    @Test
    void testValidateCsvContent_InvalidOptions() {
        String csvContent = "serZone;type;tag;description\n" +
                            "invalidSerZone;validType;validTag;validDescription";

        boolean isValid = CsvValidation.validateCsvContent(csvContent);

        assertFalse(isValid);
    }

    @Test
    void testValidateCsvContent_IOError() {
        boolean isValid = CsvValidation.validateCsvContent(null);

        assertFalse(isValid);
    }
}
