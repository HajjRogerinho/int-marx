package com.example.demo.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class CoherencyReportTest {

    @Test
    void testConstructorAndGetters() {
        String serZone = "FBMZ";
        String type = "catenaryCase without nodes";
        String tag = "";
        String description = "21815";
        String comment = "";

        CoherencyReport report = new CoherencyReport(serZone, type, tag, description, comment);

        assertEquals(serZone, report.getSerZone());
        assertEquals(type, report.getType());
        assertEquals(tag, report.getTag());
        assertEquals(description, report.getDescription());
        assertEquals(comment, report.getComment());
        assertNull(report.getId());
    }

    @Test
    void testSetters() {
        CoherencyReport report = new CoherencyReport();

        String serZone = "FBMZ";
        String type = "catenaryCase without nodes";
        String tag = "";
        String description = "21815";
        String comment = "";

        report.setSerZone(serZone);
        report.setType(type);
        report.setTag(tag);
        report.setDescription(description);
        report.setComment(comment);

        assertEquals(serZone, report.getSerZone());
        assertEquals(type, report.getType());
        assertEquals(tag, report.getTag());
        assertEquals(description, report.getDescription());
        assertEquals(comment, report.getComment());
    }

    @Test
    void testSetAndGetId() {
        CoherencyReport report = new CoherencyReport();
        Integer id = 1;
        report.setId(id);
        assertEquals(id, report.getId());
    }
}
