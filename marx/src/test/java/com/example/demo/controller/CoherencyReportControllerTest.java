package com.example.demo.controller;

import com.example.demo.model.CoherencyReport;
import com.example.demo.repository.CoherencyReportRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

class CoherencyReportControllerTest {

    @Mock
    private CoherencyReportRepository coherencyReportRepository;

    @InjectMocks
    private CoherencyReportController coherencyReportController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAllCoherencyReports() {
        List<CoherencyReport> reports = new ArrayList<>();
        reports.add(new CoherencyReport("FBMZ", "blockSignal without node", "", "L-96E-BZ98", ""));
        when(coherencyReportRepository.findAll()).thenReturn(reports);

        ResponseEntity<List<CoherencyReport>> response = coherencyReportController.getAllCoherencyReports();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, response.getBody().size());
    }

    @Test
    void testGetAllCoherencyReportsEmptyList() {
        when(coherencyReportRepository.findAll()).thenReturn(new ArrayList<>());

        ResponseEntity<List<CoherencyReport>> response = coherencyReportController.getAllCoherencyReports();

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        assertEquals(0, response.getBody() != null ? response.getBody().size() : 0);
    }

    @Test
    void testGetAllCoherencyReportsError() {
        when(coherencyReportRepository.findAll()).thenThrow(new RuntimeException("Database connection error"));

        ResponseEntity<List<CoherencyReport>> response = coherencyReportController.getAllCoherencyReports();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    @Test
    void testGetCoherencyReportByIdFound() {
        CoherencyReport mockReport = new CoherencyReport("FBMZ", "blockSignal without node", "", "L-96E-BZ98", "");
        Optional<CoherencyReport> optionalReport = Optional.of(mockReport);

        when(coherencyReportRepository.findById(mockReport.getId())).thenReturn(optionalReport);

        ResponseEntity<CoherencyReport> response = coherencyReportController.getCoherencyReportById(mockReport.getId());

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(mockReport, response.getBody());
    }

    @Test
    void testGetCoherencyReportByIdNotFound() {
        when(coherencyReportRepository.findById(123456789)).thenReturn(Optional.empty());

        ResponseEntity<CoherencyReport> response = coherencyReportController.getCoherencyReportById(123456789);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    void testSearchByAttributesFound() {
        String serZone = "exampleSerZone";
        String type = "exampleType";
        String tag = "exampleTag";
        String description = "exampleDescription";

        List<CoherencyReport> mockReports = new ArrayList<>();
        mockReports.add(new CoherencyReport(serZone, type, tag, description, ""));

        when(coherencyReportRepository.searchByAttributes(serZone, type, tag, description)).thenReturn(mockReports);

        ResponseEntity<List<CoherencyReport>> response = coherencyReportController.searchByAttributes(serZone, type, tag, description);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(mockReports, response.getBody());
    }

    @Test
    void testSearchByAttributesNotFound() {
        String serZone = "exampleSerZone";
        String type = "exampleType";
        String tag = "exampleTag";
        String description = "exampleDescription";

        when(coherencyReportRepository.searchByAttributes(serZone, type, tag, description)).thenReturn(new ArrayList<>());

        ResponseEntity<List<CoherencyReport>> response = coherencyReportController.searchByAttributes(serZone, type, tag, description);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        assertEquals(0, response.getBody() != null ? response.getBody().size() : 0);
    }

    @Test
    void testSearchByAttributesError() {
        String serZone = "exampleSerZone";
        String type = "exampleType";
        String tag = "exampleTag";
        String description = "exampleDescription";

        when(coherencyReportRepository.searchByAttributes(serZone, type, tag, description))
                .thenThrow(new RuntimeException("Error while searching"));

        ResponseEntity<List<CoherencyReport>> response = coherencyReportController.searchByAttributes(serZone, type, tag, description);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    void testCreateCoherencyReportSuccess() {
        CoherencyReport mockReport = new CoherencyReport("FBMZ", "blockSignal without node", "", "L-96E-BZ98", "");

        when(coherencyReportRepository.save(any(CoherencyReport.class))).thenReturn(mockReport);

        ResponseEntity<CoherencyReport> response = coherencyReportController.createCoherencyReport(mockReport);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(mockReport, response.getBody());
    }

    @Test
    void testCreateCoherencyReportError() {
        CoherencyReport mockReport = new CoherencyReport("FBMZ", "blockSignal without node", "", "L-96E-BZ98", "");

        when(coherencyReportRepository.save(any(CoherencyReport.class))).thenThrow(new RuntimeException("Failed to save"));

        ResponseEntity<CoherencyReport> response = coherencyReportController.createCoherencyReport(mockReport);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    void testCreateCoherencyReportsFromCsvValid() {
        String mockCsvContent = "serZone;type;tag;description\n" +
                                "FBMZ;catenaryCase without nodes;;21815\n" +
                                "FBMZ;catenaryCase without nodes;;21930";

        List<CoherencyReport> mockReports = new ArrayList<>();
        mockReports.add(new CoherencyReport("FBMZ","catenaryCase without nodes","","21815",""));
        mockReports.add(new CoherencyReport("FBMZ","catenaryCase without nodes","","21930",""));

        when(coherencyReportRepository.save(any(CoherencyReport.class))).thenReturn(mockReports.get(0), mockReports.get(1));

        ResponseEntity<List<CoherencyReport>> response = coherencyReportController.createCoherencyReportsFromCsv(mockCsvContent);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(mockReports, response.getBody());
    }

    @Test
    void testCreateCoherencyReportsFromCsvInvalid() {
        String invalidCsvContent = "invalid,csv,content";

        ResponseEntity<List<CoherencyReport>> response = coherencyReportController.createCoherencyReportsFromCsv(invalidCsvContent);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    void testUpdateCoherencyReportFound() {
        CoherencyReport mockReport = new CoherencyReport("FBMZ", "blockSignal without node", "", "L-96E-BZ98", "");

        Optional<CoherencyReport> optionalReport = Optional.of(mockReport);
        when(coherencyReportRepository.findById(mockReport.getId())).thenReturn(optionalReport);

        CoherencyReport updatedReport = new CoherencyReport("FBMZ", "catenaryCase without nodes", "FIXED", "21815", "comment");
        updatedReport.setId(mockReport.getId());

        when(coherencyReportRepository.save(any(CoherencyReport.class))).thenReturn(updatedReport);
        ResponseEntity<CoherencyReport> response = coherencyReportController.updateCoherencyReport(mockReport.getId(), updatedReport);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(updatedReport.getSerZone(), response.getBody().getSerZone());
        assertEquals(updatedReport.getType(), response.getBody().getType());
        assertEquals(updatedReport.getTag(), response.getBody().getTag());
        assertEquals(updatedReport.getDescription(), response.getBody().getDescription());
        assertEquals(updatedReport.getComment(), response.getBody().getComment());
    }

    @Test
    void testUpdateCoherencyReportNotFound() {
        CoherencyReport mockReport = new CoherencyReport("FBMZ", "blockSignal without node", "", "L-96E-BZ98", "");

        when(coherencyReportRepository.findById(123456789)).thenReturn(Optional.empty());

        CoherencyReport updatedReport = new CoherencyReport("FBMZ", "catenaryCase without nodes", "FIXED", "21815", "comment");
        updatedReport.setId(mockReport.getId());

        ResponseEntity<CoherencyReport> response = coherencyReportController.updateCoherencyReport(mockReport.getId(), updatedReport);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNull(response.getBody());
    }

    @Test
    void testDeleteCoherencyReportFound() {
        CoherencyReport mockReport = new CoherencyReport("FBMZ", "blockSignal without node", "", "L-96E-BZ98", "");

        doNothing().when(coherencyReportRepository).deleteById(mockReport.getId());

        ResponseEntity<HttpStatus> response = coherencyReportController.deleteCoherencyReport(mockReport.getId());

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void testDeleteCoherencyReportNotFound() {
        doThrow(EmptyResultDataAccessException.class).when(coherencyReportRepository).deleteById(123456789);

        ResponseEntity<HttpStatus> response = coherencyReportController.deleteCoherencyReport(123456789);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    @Test
    void testDeleteAllCoherencyReportsSuccess() {
        doNothing().when(coherencyReportRepository).deleteAll();

        ResponseEntity<HttpStatus> response = coherencyReportController.deleteAllCoherencyReports();

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void testDeleteAllCoherencyReportsError() {
        doThrow(RuntimeException.class).when(coherencyReportRepository).deleteAll();

        ResponseEntity<HttpStatus> response = coherencyReportController.deleteAllCoherencyReports();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }
}
