package com.example.demo.utility;

import com.example.demo.model.CoherencyReport;
import java.util.List;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CsvUtilityTest {

    @Test
    void testConvertCsvToJson() {
        String csvContent = "serZone;type;tag;description\n" +
                            "FBMZ;catenaryCase without nodes;;21815\n" +
                            "FBMZ;catenaryCase without nodes;;21930";

        String json = CsvUtility.convertCsvToJson(csvContent);

        assertTrue(json.contains("serZone") && json.contains("FBMZ") &&
                   json.contains("type") && json.contains("catenaryCase without nodes") &&
                   json.contains("tag") && json.contains("21815") &&
                   json.contains("21930"));
    }

    @Test
    void testConvertJsonToList() {
        String json = "[{\"serZone\":\"FBMZ\",\"type\":\"catenaryCase without nodes\",\"tag\":\"\",\"description\":21815}," +
                      "{\"serZone\":\"FBMZ\",\"type\":\"catenaryCase without nodes\",\"tag\":\"\",\"description\":21930}]";

        List<CoherencyReport> coherencyReports = CsvUtility.convertJsonToList(json);

        assertEquals(2, coherencyReports.size());
        assertEquals("FBMZ", coherencyReports.get(0).getSerZone());
        assertEquals("catenaryCase without nodes", coherencyReports.get(0).getType());
        assertEquals("", coherencyReports.get(0).getTag());
        assertEquals("21815", coherencyReports.get(0).getDescription());

        assertEquals("FBMZ", coherencyReports.get(1).getSerZone());
        assertEquals("catenaryCase without nodes", coherencyReports.get(1).getType());
        assertEquals("", coherencyReports.get(1).getTag());
        assertEquals("21930", coherencyReports.get(1).getDescription());
    }

    @Test
    void testConvertJsonToListEmpty() {
        String json = "invalid json";

        List<CoherencyReport> coherencyReports = CsvUtility.convertJsonToList(json);
        assertTrue(coherencyReports.isEmpty());
    }

    @Test
    void testConvertCsvToJson_ErrorReadingFile() {
        String json = CsvUtility.convertCsvToJson(null);

        assertEquals("Error: Unable to read the file.", json);
    }
}
